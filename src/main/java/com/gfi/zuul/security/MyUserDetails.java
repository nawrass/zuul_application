package com.gfi.zuul.security;


import com.gfi.zuul.dao.UserRepository;
import com.gfi.zuul.model.Role;
import com.gfi.zuul.model.RoleName;
import com.gfi.zuul.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class MyUserDetails implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        final User user = userRepository.findByEmail(email).get();

        if (user == null) {
            throw new UsernameNotFoundException("email '" + email + "' not found");
        }
        List<RoleName> roleNames=new ArrayList<>();
        for(Role role:user.getRoles()){
            roleNames.add(role.getName());
        }
        return org.springframework.security.core.userdetails.User//
                .withUsername(email)//
                .password(user.getPassword())//
                .authorities(roleNames)//
                .accountExpired(false)//
                .accountLocked(false)//
                .credentialsExpired(false)//
                .disabled(false)//
                .build();
    }

}
