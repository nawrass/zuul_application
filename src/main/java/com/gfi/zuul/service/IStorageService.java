package com.gfi.zuul.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface IStorageService {

    void store(MultipartFile file, Long id_user);

    public Resource loadFile(String filename);

    public void deleteAll();

    public void init();
}
